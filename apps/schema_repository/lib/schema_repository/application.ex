defmodule SchemaRepository.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      Registry.start_link(keys: :unique, name: SchemaRepository.Repo),
      {:via, Registry, {SchemaRepository.Repo, %{}}},
      {SchemaRepository.Crud}
      # Starts a worker by calling: SchemaRepository.Worker.start_link(arg)
      # {SchemaRepository.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SchemaRepository.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
