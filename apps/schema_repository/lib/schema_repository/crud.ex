defmodule SchemaRepository.Crud do
  use Agent

  @moduledoc """
    SchemaRepository.Crud handles adding, updating and deleting schemas by identifier.
    Currently only Avro schemas are supported in the system.
  """
  @moduledoc since: "0.1.0"

  @type repo :: %{optional(String.t()) => AvroEx.Schema.t()}

  @spec start_link(repo) :: {atom(), any()}
  @doc """
  Initilize the repository with a map defined by the `repo` type
  """
  def start_link(initial_value) do
    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  @spec add(String.t(), String.t()) :: {atom(), String.t()}
  @doc ~S"""
    `add/2` is used to add a validated schema to the cache by identifier and avro formatted json schema
  ## Examples
      # Successful usage returns `{:ok, <identifer>}` to confirm the insert
      iex> {:ok, _} = SchemaRepository.Crud.start_link(%{})
      ...> schema = "{ \"type\": \"record\", \"name\": \"LinkedList\", \"fields\": [ {\"name\": \"value\", \"type\": \"int\"}, {\"name\": \"next\", \"type\": [\"null\", \"LinkedList\"]} ] }"
      ...> SchemaRepository.Crud.add("identifier", schema)
      {:ok, "identifier"}

      # Failure returns an expected `{:error, "invalid input"}` tuple on fundimentally bad data
      iex> {:ok, _} = SchemaRepository.Crud.start_link(%{})
      ...> SchemaRepository.Crud.add(7, "schema")
      {:error, "invalid input"}

      # Failure returns an expected `{:error, "invalid json schema"}` tuple on an invalid json string
      iex> {:ok, _} = SchemaRepository.Crud.start_link(%{})
      ...> SchemaRepository.Crud.add("identifer", "schema")
      {:error, "invalid json schema"}

      # Failure returns as expected if the key is already in use
      iex> {:ok, _} = SchemaRepository.Crud.start_link(%{"identifer" => %AvroEx.Schema{}})
      ...> schema = "{ \"type\": \"record\", \"name\": \"LinkedList\", \"fields\": [ {\"name\": \"value\", \"type\": \"int\"}, {\"name\": \"next\", \"type\": [\"null\", \"LinkedList\"]} ] }"
      ...> SchemaRepository.Crud.add("identifer", schema)
      {:error, "identifier already in use"}
  """
  def add(identifier, schema) when is_bitstring(identifier) and is_bitstring(schema) do
    schema
    |> verify_avro_format
    |> add_to_registry(identifier)
    |> handle_result
  end

  def add(_, _) do
    {:error, "invalid input"}
  end

  defp verify_avro_format(json_string) do
    try do
      AvroEx.parse_schema(json_string)
    rescue
      _ -> {:error, "invalid json schema"}
    end
  end

  defp handle_result({:ok, identifer}) do
    {:ok, identifer}
  end

  defp handle_result({:error, status}) do
    {:error, status}
  end

  defp handle_result(result) do
    {:error, result}
  end

  defp add_to_registry({:ok, %AvroEx.Schema{} = schema}, identifer)
       when is_bitstring(identifer) do
    guard_from_overwrite(identifer, schema)
    |> insert
  end

  defp add_to_registry({:error, {:invalid, _, _}}, _identifer) do
    {:error, "invalid json schema"}
  end

  defp add_to_registry(error_case, _identifer), do: passthrough(error_case)

  defp guard_from_overwrite(identifer, schema) do
    status =
      Agent.get(__MODULE__, fn state ->
        !is_nil(state[identifer])
      end)

    {status, identifer, schema}
  end

  defp insert({false, identifer, schema}) do
    Agent.update(__MODULE__, fn state_map ->
      Map.merge(state_map, %{identifer => schema})
    end)

    {:ok, identifer}
  end

  defp insert({true, _identifier, _schema}) do
    {:error, "identifier already in use"}
  end

  defp passthrough(other), do: other
end
