defmodule SyncAsyncEx.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      version: "0.1.0-dev",
      deps: deps(),
      package: [
        description:
          "An elixir project for providing synchronous backed by an asynchronous system for immutable, ledger based infrastructure.",
        licenses: ["GNUV3"],
        maintainers: ["Jeff Kantarek"],
        links: %{gitlab: "https://gitlab.com/jkantarek/sync_async_ex"}
      ],

      # Docs
      name: "SyncAsyncEx",
      source_url: "https://gitlab.com/jkantarek/sync_async_ex",
      homepage_url: "https://jkantarek.gitlab.io/sync_async_ex/readme.html",
      docs: [
        # The main page in the docs
        main: "readme",
        extras: ["README.md"],
        output: "public"
      ]
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:distillery, "~> 2.0", runtime: false},
      {:ex_doc, "~> 0.20", only: :dev}
    ]
  end
end
